import 'package:example_project/core/network/network_info.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'network_info_test.mocks.dart';

@GenerateMocks([
  InternetConnectionChecker,
])
void main() {
  MockInternetConnectionChecker connectionChecker = MockInternetConnectionChecker();
  NetworkInfoImpl networkInfoImpl = NetworkInfoImpl(connectionChecker);

  group('isConnected', () {
    test('should forward the call to InternetConnectionChecker.hasConnection',
        () async {
      // connectionChecker = MockInternetConnectionChecker();
      // networkInfoImpl = NetworkInfoImpl(connectionChecker);

      // arrange
      when(connectionChecker.hasConnection).thenAnswer((_) async => true);
      // act
      final result = await networkInfoImpl.isConnected;
      // assert
      verify(connectionChecker.hasConnection);
      expect(result, true);
    });
  });
}
