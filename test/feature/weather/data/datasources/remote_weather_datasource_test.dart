import 'package:example_project/core/error/exceptions.dart';
import 'package:example_project/features/weather/data/datasources/remote_weather_datasource.dart';
import 'package:example_project/features/weather/data/models/weather_data.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'package:http/http.dart' as http;
import '../../../../fixtures/fixture_reader.dart';
import 'remote_weather_datasource_test.mocks.dart';

@GenerateMocks([http.Client, WeatherData])
void main() {
  late MockClient client;
  late RemoteWeatherDatasourceImpl datasource;

  setUp(() {
    client = MockClient();
    datasource = RemoteWeatherDatasourceImpl(client: client);
  });

  void setUpMockHttpClientGetSuccess() {
    when(client.get(
      any,
      headers: anyNamed('headers'),
    )).thenAnswer(
        (_) async => http.Response(fixture('weather/response.json'), 200));
  }

  void setUpMockHttpClientGetServerError() {
    when(client.get(
      any,
      headers: anyNamed('headers'),
    )).thenAnswer(
            (_) async => http.Response('', 400));
  }

  void setUpMockHttpClientGetNotAuthorizedException() {
    when(client.get(
      any,
      headers: anyNamed('headers'),
    )).thenAnswer(
            (_) async => http.Response('', 403));
  }

  group('getWeather', () {
    test('should perform a GET request on a URL with application/json header',
        () async {
      setUpMockHttpClientGetSuccess();

      await datasource.getWeather();

      verify(client.get(Uri(
          path: 'api.openweathermap.org/data/2.5/weather',
          queryParameters: {'q': 'London', 'appid': 'appid'})));
    });

    test('should return a [WeatherData] object when response coed == 200',
        () async {
      setUpMockHttpClientGetSuccess();

      final result = await datasource.getWeather();

      expect(result, isA<WeatherData>());
      expect(result.params.temperature, 282.55);
    });

    test('should return an [ServerException] if response status code != 200', () async {
      setUpMockHttpClientGetServerError();

      final call = datasource.getWeather;

      expect(() => call(), throwsA(isA<ServerException>()));
    });

    test('should return an [NotAuthorizedException] if response status code == 403', () async {
      setUpMockHttpClientGetNotAuthorizedException();

      final call = datasource.getWeather;

      expect(() => call(), throwsA(isA<NotAuthorizedException>()));
    });
  });
}
