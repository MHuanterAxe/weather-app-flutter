import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:example_project/core/error/exceptions.dart';
import 'package:example_project/core/error/failures.dart';
import 'package:example_project/core/network/network_info.dart';
import 'package:example_project/features/weather/data/datasources/remote_weather_datasource.dart';
import 'package:example_project/features/weather/data/models/weather_data.dart';
import 'package:example_project/features/weather/data/repositories/weather_data_repository.dart';
import 'package:example_project/features/weather/domain/repositories/weather_data_repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../../../fixtures/fixture_reader.dart';
import 'weather_data_repository_test.mocks.dart';

@GenerateMocks([
  RemoteWeatherDatasource,
  NetworkInfo,
])
void main() {
  late RemoteWeatherDatasource datasource;
  late NetworkInfo networkInfo;

  late WeatherDataRepository repository;

  setUp(() {
    datasource = MockRemoteWeatherDatasource();
    networkInfo = MockNetworkInfo();

    repository = WeatherDataRepositoryImpl(
      remoteDatasource: datasource,
      networkInfo: networkInfo,
    );
  });

  void runTestsOnline(Function body) {
    group('device is online', () {
      setUp(() {
        when(networkInfo.isConnected)..thenAnswer((_) async => true);
      });

      body();
    });
  }

  void runTestsOffline(Function body) {
    group('device is offline', ()
    {
      setUp(() {
        when(networkInfo.isConnected)
          ..thenAnswer((_) async => false);
      });

      body();
    });
  }


  group('getWeatherData', () {
   test('should check if device online', () async {
     when(datasource.getWeather()).thenAnswer((_) async =>
         WeatherData.fromJson(jsonDecode(fixture('weather/response.json'))));

     when(networkInfo.isConnected).thenAnswer((_) async => true);
      // act
      repository.getWeatherData();
      // assert
      verify(networkInfo.isConnected);
    });

    runTestsOffline(() {
      test(
        'should return no network failure when the device is offline',
            () async {
          // arrange
          when(datasource.getWeather())
              .thenThrow(NoNetworkException());
          // act
          final result = await repository.getWeatherData();
          // assert
          // verifyZeroInteractions(datasource);
          expect(result, Left(NoNetworkFailure()));
        },
      );
    });

  });
}
