import 'package:example_project/pages/home_page.dart';
import 'package:example_project/themes/light.dart';
import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (context, child) => ResponsiveWrapper.builder(
        child,
        maxWidth: 1200,
        minWidth: 480,
        defaultScale: true,
        background: Container(
          color: Colors.white,
        ),
        breakpoints: [
          ResponsiveBreakpoint.autoScale(400, name: 'MOBILE'),
          ResponsiveBreakpoint.autoScale(800, name: 'TABLET'),
          ResponsiveBreakpoint.autoScale(1000, name: 'DESKTOP'),
        ],
      ),
      title: 'Flutter Demo',
      darkTheme: ThemeData(
        fontFamily: 'Rubic',
        backgroundColor: Colors.black45,
      ),
      themeMode: ThemeMode.light,
      debugShowCheckedModeBanner: false,
      theme: lightTheme,
      home: HomePage(),
    );
  }
}