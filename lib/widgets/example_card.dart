import 'package:flutter/material.dart';

class ExampleCard extends StatelessWidget {
  const ExampleCard(
      {Key? key,
      required this.child,
      this.color = Colors.white,
      this.padding = const EdgeInsets.all(20.0),
      this.borderRadius = const BorderRadius.all(Radius.elliptical(20, 20))})
      : super(key: key);

  final Widget child;
  final Color color;
  final EdgeInsetsGeometry padding;
  final BorderRadiusGeometry borderRadius;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      decoration: BoxDecoration(
          color: color,
          borderRadius: borderRadius,
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: const Color.fromARGB(16, 0, 0, 0),
              blurRadius: 20.0,
              offset: Offset(5.0, 5.0),
              spreadRadius: 5.0,
            )
          ]),
      child: child,
    );
  }
}
