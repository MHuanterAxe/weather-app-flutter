import 'package:dartz/dartz.dart';
import 'package:example_project/core/error/failures.dart';
import 'package:example_project/features/weather/data/models/weather_data.dart';

abstract class WeatherDataRepository {
  Future<Either<Failure, WeatherData>> getWeatherData();
}