import 'package:dartz/dartz.dart';
import 'package:example_project/core/error/exceptions.dart';
import 'package:example_project/core/error/failures.dart';
import 'package:example_project/core/network/network_info.dart';
import 'package:example_project/features/weather/data/datasources/remote_weather_datasource.dart';
import 'package:example_project/features/weather/data/models/weather_data.dart';
import 'package:example_project/features/weather/domain/repositories/weather_data_repository.dart';

class WeatherDataRepositoryImpl implements WeatherDataRepository {
  final RemoteWeatherDatasource remoteDatasource;
  final NetworkInfo networkInfo;

  WeatherDataRepositoryImpl({
    required this.remoteDatasource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, WeatherData>> getWeatherData() async {
    final hasConnection = await networkInfo.isConnected;
    if (hasConnection) {
      try {
        final result = await remoteDatasource.getWeather();

        return Right(result);
      } on ServerException {
        return Left(ServerFailure());
      } on NotAuthorizedException {
        return Left(NotAuthorizedFailure());
      }
    }

    return Left(NoNetworkFailure());
  }
}
