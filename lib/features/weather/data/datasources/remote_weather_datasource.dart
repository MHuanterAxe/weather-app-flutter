import 'dart:convert';

import 'package:example_project/core/error/exceptions.dart';
import 'package:example_project/features/weather/data/models/weather_data.dart';
import 'package:http/http.dart' as http;

abstract class RemoteWeatherDatasource {
  Future<WeatherData> getWeather();
}

class RemoteWeatherDatasourceImpl implements RemoteWeatherDatasource {
  final http.Client client;

  RemoteWeatherDatasourceImpl({
    required this.client,
  });

  @override
  Future<WeatherData> getWeather() async {
    final response = await client.get(Uri(
        path: 'api.openweathermap.org/data/2.5/weather',
        queryParameters: {
          'q': 'London',
          'appid': 'appid'
        }
    ));
    
    if (response.statusCode == 403) {
      throw NotAuthorizedException();
    }
    
    if (response.statusCode != 200) {
      throw ServerException();
    }

    final json = jsonDecode(response.body);

    return Future.value(WeatherData.fromJson(json));
  }
}
