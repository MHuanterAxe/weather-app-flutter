import 'package:example_project/features/weather/data/models/coordinates.dart';
import 'package:example_project/features/weather/data/models/weather_params.dart';
import 'package:json_annotation/json_annotation.dart';

import 'weather.dart';
import 'wind.dart';

part 'weather_data.g.dart';

@JsonSerializable()
class WeatherData{

  @JsonKey(name: 'coord')
  final CoordinatesModel coordinates;

  final List<Weather> weather;

  @JsonKey(name: 'main')
  final WeatherParams params;

  final Wind wind;

  final int dt;

  WeatherData({
    required this.coordinates,
    required this.weather,
    required this.params,
    required this.wind,
    required this.dt
  });

  factory WeatherData.fromJson(Map<String, dynamic> json) =>
      _$WeatherDataFromJson(json);
}
