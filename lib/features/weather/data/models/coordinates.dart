import 'package:json_annotation/json_annotation.dart';

part 'coordinates.g.dart';

@JsonSerializable()
class CoordinatesModel {
  final double lon;
  final double lat;

  CoordinatesModel({
    required this.lon,
    required this.lat,
  });

  factory CoordinatesModel.fromJson(Map<String, dynamic> json) => _$CoordinatesModelFromJson(json);
}
