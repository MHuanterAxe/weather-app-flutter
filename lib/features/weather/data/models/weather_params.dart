import 'package:json_annotation/json_annotation.dart';

part 'weather_params.g.dart';

@JsonSerializable()
class WeatherParams {
  @JsonKey(name: 'temp')
  final double temperature;

  final double feels_like;

  @JsonKey(name: 'temp_min')
  final double temperature_min;

  @JsonKey(name: 'temp_max')
  final double temperature_max;

  final double pressure;

  final double humidity;

  WeatherParams({
    required this.temperature,
    required this.temperature_max,
    required this.temperature_min,
    required this.feels_like,
    required this.pressure,
    required this.humidity,
  });

  factory WeatherParams.fromJson(Map<String, dynamic> json) =>
      _$WeatherParamsFromJson(json);
}
