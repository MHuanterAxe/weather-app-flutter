import 'package:json_annotation/json_annotation.dart';

part 'weather.g.dart';

@JsonSerializable()
class Weather {

  final int id;

  @JsonKey(name: 'main')
  final String name;

  final String description;

  Weather({ required this.id, required this.name, required this.description });

  factory Weather.fromJson(Map<String, dynamic> json) => _$WeatherFromJson(json);
}