import 'package:flutter/material.dart';

final ThemeData lightTheme = ThemeData(
  primarySwatch: Colors.green,
  fontFamily: 'Rubic',
  textTheme: TextTheme(
    headline1: TextStyle(
      fontSize: 32.0,
      color: Colors.black,
    ),
    headline2: TextStyle(
      fontSize: 28.0,
      color: Colors.black45,
      fontWeight: FontWeight.w700,
    ),
    bodyText1: TextStyle(
      fontSize: 16.0,
      color: Colors.black26,
      fontWeight: FontWeight.w600,
    ),
    bodyText2: TextStyle(
      fontSize: 16.0,
      color: Colors.black,
      fontWeight: FontWeight.w600,
    ),
  ),
);
