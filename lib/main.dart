import 'dart:io';

import 'package:logging/logging.dart';
import 'package:desktop_window/desktop_window.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'injection_container.dart';
import 'app.dart';

void main() async {
  _setupLogging();

  configureDependencies();

  WidgetsFlutterBinding.ensureInitialized();
  if (!kIsWeb && (Platform.isWindows || Platform.isMacOS || Platform.isLinux)) {
    await DesktopWindow.setMinWindowSize(const Size(480, 500));
  }
  runApp(MyApp());
}


void _setupLogging() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((rec) {
    print('[${rec.level.name}] |${rec.time}| ${rec.message}');
  });
}
