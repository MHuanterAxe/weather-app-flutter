import 'dart:io';

import 'package:injectable/injectable.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

abstract class NetworkInfo {
  Future<bool> get isConnected;
}

@Singleton(as: NetworkInfo)
class NetworkInfoImpl implements NetworkInfo {
  final InternetConnectionChecker connectionChecker;

  NetworkInfoImpl(this.connectionChecker) {
    this.connectionChecker.addresses = [
      AddressCheckOptions(InternetAddress('1.1.1.1')),
      AddressCheckOptions(InternetAddress('8.8.4.4')),
    ];
  }

  @override
  Future<bool> get isConnected async =>
      await this.connectionChecker.hasConnection;
}
