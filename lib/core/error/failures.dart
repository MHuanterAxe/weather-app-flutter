import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  @override
  List<Object> get props => [];
}


class NoNetworkFailure extends Failure {}
class ServerFailure extends Failure {}
class NotAuthorizedFailure extends Failure {}