import 'package:example_project/widgets/example_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final weatherInfoItems = <Map<String, String>>[
    {
      'label': 'Температура',
      'value': '+27',
    },
    {
      'label': 'Ощущается как',
      'value': '+29',
    },
    {
      'label': 'Влажность',
      'value': '20%',
    },
    {
      'label': 'Давление',
      'value': '765 мм',
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          constraints: BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width,
            maxHeight: double.infinity,
          ),
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Text(
                      'Главная',
                      style: Theme.of(context).textTheme.headline1,
                    ),
                  )
                ],
              ),
              ExampleCard(
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            'Ясно',
                            style: Theme.of(context).textTheme.headline2,
                          ),
                        ),
                        Container(
                          child: SvgPicture.asset(
                            'assets/icons/clear.svg',
                            width: 40.0,
                          ),
                        ),
                      ],
                    ),
                    GridView.builder(
                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 20.0,
                        mainAxisSpacing: 20,
                        childAspectRatio: 2,
                      ),
                      itemCount: weatherInfoItems.length,
                      itemBuilder: (context, index) {
                        return WeatherInfoGridTile(
                          label: weatherInfoItems[index]['label'].toString(),
                          value: weatherInfoItems[index]['value'].toString(),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class WeatherInfoGridTile extends StatelessWidget {
  const WeatherInfoGridTile({
    Key? key, required this.label, this.value = '0', this.icon = Icons.cloud,
  }) : super(key: key);

  final String label;
  final String value;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              this.value,
              style: Theme.of(context).textTheme.headline2,
            ),
            Icon(
              this.icon,
              size: 20.0,
            ),
          ],
        ),
        SizedBox(height: 10.0,),
        Text(
          this.label,
          style: Theme.of(context).textTheme.bodyText1,
        ),
      ],
    );
  }
}
