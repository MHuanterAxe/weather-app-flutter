import 'package:example_project/pages/home_page.dart';
import 'package:flutter/material.dart';

class AppRouter {
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (context) => HomePage());
      default: return MaterialPageRoute(builder: (context) => HomePage());
    }
  }
}
