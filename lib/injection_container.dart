import 'package:injectable/injectable.dart';
import 'package:get_it/get_it.dart';

import 'injection_container.config.dart';

final getIt = GetIt.instance;

@InjectableInit(
  initializerName: r'$initGetIt', // default
  preferRelativeImports: true, // default
  asExtension: false, // default
)
void configureDependencies() => $initGetIt(getIt);

// Future<void> init () async {
//   ///----------------FEATURES----------------
//   ///-------------Authentication-------------
//   /// Bloc
//
//   /// UseCases
//   /// Repository
//
//   /// Data Sources
//   ///------------------CORE------------------
//   /// Network Info
//   sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));
//
//
//   ///----------------EXTERNAL----------------
//   /// Shared Preferences
//   final sharedPreferences = await SharedPreferences.getInstance();
//   sl.registerLazySingleton<SharedPreferences>(() => sharedPreferences);
//
//   /// Http
//   sl.registerLazySingleton(() => http.Client());
//
//   /// Data Connection Checker
//   sl.registerLazySingleton(() => InternetConnectionChecker());
// }
